#!/bin/bash
NAME="graficos_server" # Name of the application
echo "Starting $NAME as `whoami`"
source /home/webhooks/pitperu_env/bin/activate
uvicorn web:app --port=9090