import redis
import requests
from bs4 import BeautifulSoup
from config import *

timeout = 555

r = redis.StrictRedis(**redis_config)


def get_cookies():

    redis_key = 'cache_cookie_login'
    if r.get(redis_key):
        return str(r.get(redis_key))

    cookies = {}
    session = requests.Session()
    response = session.post('%slogin' % url_servicio,
        data = {'username':'jovan','password':'Multifiber2020*'}, timeout=timeout
    )

    c = session.cookies.get_dict()
    key = c.get('laravel_session')
    r.setex(
        redis_key,
        cache_time_cookie, # time to store data
        key
    )
    return key

def get_img_member(id):
    id = int(id)
    redis_key = 'cache_grafico_general_%d' % id 
    if r.get(redis_key):
        return {
            'src': str(r.get(redis_key))
        }

    url = '{}customer/overview/{}'.format(url_servicio, id)
    headers = {'Content-Type': 'application/xml'}
    cookies = {'laravel_session': get_cookies()}
    response = requests.get(url, headers=headers, timeout=timeout, cookies=cookies)
    contexto = {}
    if response.status_code == 200:
        soup = BeautifulSoup(response.text, "html.parser")
        div = soup.find("div", {"id": "overview"})
        img = div.find('img')
        src = img['src']
        contexto = {
            'src':src
        }
        r.setex(
            redis_key,
            cache_time, # time to store data
            src
        )

    return contexto

def get_img_pitperu():
    url = '%sadmin/'%url_servicio
    headers = {'Content-Type': 'application/xml'}
    cookies = {'laravel_session': get_cookies()}
    response = requests.get(url, headers=headers, timeout=timeout, cookies=cookies)
    contexto = {}
    if response.status_code == 200:
        soup = BeautifulSoup(response.text, "html.parser")
        div = soup.find("div", {"class": "card-body"})
        img = div.find('img')
        src = img['src']
        contexto = {
            'src':src
        }
    return contexto



def main():
    img = get_img_member(25)
    print ('<img src="{}">'.format(img['src']))


if __name__ == '__main__':
    main()