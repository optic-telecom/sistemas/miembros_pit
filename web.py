import base64
from typing import Optional
from fastapi import FastAPI, Form, Cookie
from fastapi.responses import HTMLResponse, PlainTextResponse, RedirectResponse
from iframe import *

app = FastAPI()


def responder_grafico(id_clave, user):
    
    id_clave = int(id_clave)

    if user.upper() in ['AS13335','AS263807','AS32934']:
        html = open('index_cdns.html')
        graficos = ''
        # for member_id in range(1,40):
        #     try:
        #         img = get_img_member(member_id)['src']
        #         graficos = graficos + '<img src="{}"><br>'.format(img)
        #     except Exception as e:
        #         print ('grafico no encontrado',member_id)


        html_content = html.read().replace('{}', graficos)

    else:

        html = open('index.html')
        try:
            img = get_img_member(id_clave)['src']
            html_content = html.read().replace('{}','<img src="{}">'.format(img))
        except Exception as e:
            print (e)
            html_content = html.read().replace('{}','Sin graficos para mostrar')

    return HTMLResponse(content=html_content, status_code=200)


def b64e(s):
    return base64.b64encode(s.encode()).decode()


def b64d(s):
    try:
        return base64.b64decode(s).decode()
    except Exception as e:
        return None


@app.post("/")
def login_post(usuario: str = Form(...), 
               clave: str = Form(...),
               _ga: Optional[str] = Cookie(None),
               _gat: Optional[str] = Cookie(None)):

    id_clave = False
    if len(clave) == 8:
        try:
            id_clave = int(clave[4])
        except Exception as e:
            print (e)
        
    if len(clave) == 9:
        try:
            id_clave = "%s%s"%(clave[0],clave[8])
            id_clave = int(id_clave)
        except Exception as e:
            print (e)

    if not id_clave:
        return RedirectResponse('/login/')

    if _ga and _gat:
        if _ga!='0':
            if b64d(str(_gat)) != str(_ga):
                return RedirectResponse('/login/')


    r = responder_grafico(id_clave, str(usuario))
    r.set_cookie(key="ekin", value='ekimpublic')
    r.set_cookie(key="ads", value='abk-154-784')
    r.set_cookie(key='_ga', value=str(id_clave))
    r.set_cookie(key='_gid', value='google_check_true')
    r.set_cookie(key="_gat", value=b64e(str(id_clave)))
    r.set_cookie(key='mu', value=str(usuario))
    return r


@app.get("/")
def index(
          mu: Optional[str] = Cookie(None),
          _ga: Optional[str] = Cookie(None),
          _gat: Optional[str] = Cookie(None)
    ):

    if _ga and _gat:
        if _ga!='0' and _gat !='0':
            if b64d(str(_gat)) == str(_ga):
                return responder_grafico(_ga, mu)

    html = open('login.html')
    html = html.read()
    return HTMLResponse(content=html, status_code=200)


@app.post("/login/")
def login():
    return index()


@app.get("/salir/")
def salir():
    r = RedirectResponse('/')
    r.set_cookie(key='_ga', value=0)
    r.set_cookie(key='_gat', value=1)
    r.set_cookie(key='mu', value=0)
    r.set_cookie(key='_gid', value=1)    
    return r


@app.get("/global")
def global_web():
    img = get_img_pitperu()['src'] 
    return HTMLResponse(content='<img src="{}">'.format(img), status_code=200)

@app.get("/for_cdn/{item_id}/")
def global_web(item_id,
               _ga: Optional[str] = Cookie(None),
               _gat: Optional[str] = Cookie(None)):

    if _ga and _gat:
        if _ga!='0' and _gat !='0':
            if b64d(str(_gat)) == str(_ga):
                img = get_img_member(item_id)['src']
                return HTMLResponse(content='{}'.format(img), status_code=200)

    return HTMLResponse('', status_code=401)